struct HASHNode {
    int val;
    int ref;
    struct TreeNode* root;
    UT_hash_handle hh;
 };

struct HASHNode* hashnode;

struct TreeNode* createBinaryTree(int** descriptions, int descriptionsSize, int* descriptionsColSize){
    hashnode = NULL;
    struct HASHNode *parent,*child;
    for(int i=0;i<descriptionsSize;i++){
        
        int pvalue = descriptions[i][0];
        int cvalue = descriptions[i][1];
        HASH_FIND_INT(hashnode, &cvalue, child);
        if(child==NULL){
            child = malloc(sizeof(struct HASHNode));
            child->val = cvalue;
            child->ref = 1;
            child->root = calloc(1,sizeof(struct TreeNode));
            HASH_ADD_INT(hashnode, val, child);
        }
        else{
            child->ref++;
            HASH_REPLACE_INT(hashnode, val, child,child);
        }
        
        HASH_FIND_INT(hashnode, &pvalue, parent);
        if(parent==NULL){
            parent = malloc(sizeof(struct HASHNode));
            parent->val = pvalue;
            parent->ref = 0;
            parent->root = calloc(1,sizeof(struct TreeNode));
            HASH_ADD_INT(hashnode, val, parent);
        }
        
        parent->root->val = pvalue;
        child->root->val = cvalue;
        if(descriptions[i][2]==1)
            parent->root->left = child->root;
        else
            parent->root->right = child->root;
    }
    struct TreeNode* ret = NULL;
    HASH_ITER(hh,hashnode,parent,child){
        if(parent->ref==0){
            return parent->root;
        }
    }
    return ret;
}
