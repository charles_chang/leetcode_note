
bool divideArray(int* nums, int numsSize){
    int hash[501] = {0};
    for(int i=0;i<numsSize;i++){
        hash[nums[i]]++;
    }
    for(int i=0;i<501;i++){
        if(hash[i]%2)
            return false;
    }
    return true;
}
