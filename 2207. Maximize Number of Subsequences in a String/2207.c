
long long maximumSubsequenceCount(char * text, char * pattern){
    long long numA = 0, numB = 0, res = 0;
    for(int i=0; text[i] != '\0'; ++i) {
        if(text[i] == pattern[1]) {
            numB++;
            res += numA;
        }
        if(text[i] == pattern[0]) {
            numA++;
        }
    }
    printf("%d\n", res);
    res += (numA > numB)? numA:numB;
    return res;   
}
