int** getAncestors(int n, int** edges, int edgesSize, int* edgesColSize, int* returnSize, int** returnColumnSizes){
    //int **graph = malloc(n*sizeof(int*));
    int **hist = malloc(n*sizeof(int*));
    int **ret = malloc(n*sizeof(int*));
    int *finish = calloc(n,sizeof(int));
    int *ancestor = calloc(n,sizeof(int));
    int *bfs = calloc(n,sizeof(int));
    int bfsidx = 0;
    * returnSize = n;
    *returnColumnSizes = malloc(n*sizeof(int));
    for(int i=0;i<n;i++){
        hist[i] = calloc(n,sizeof(int));
        ret[i] = calloc(n,sizeof(int));
        //graph[i] = calloc(n,sizeof(int));
        (*returnColumnSizes)[i] = 0;
    }
    for(int i=0;i<edgesSize;i++){
        //graph[edges[i][0]][edges[i][1]] = 1;
        hist[edges[i][1]][edges[i][0]] = 1;
        ancestor[edges[i][1]]++;
    }
    for(int i=0;i<n;i++){
        if(ancestor[i]==0){
            bfs[bfsidx++] = i;
        }
    }
    int rear = 0;
    while(rear!=bfsidx){
        int targetidx = bfs[rear];
        for(int i=0;i<n;i++){
            if(hist[i][targetidx]==1){
            //if(graph[targetidx][i]==1){
                for(int j=0;j<n;j++){//hist[target]->hist[i]
                    hist[i][j]|=hist[targetidx][j];
                }
                //hist[i][targetidx] = 1;
                ancestor[i]--;
                if(ancestor[i]==0){
                    //qsort
                    for(int j=0;j<n;j++){
                        if(hist[i][j]==1)
                            ret[i][((*returnColumnSizes)[i])++] = j;
                    }
                    bfs[bfsidx++] = i;
                }
            }
        }
        rear++;
    }
    return ret;
}
