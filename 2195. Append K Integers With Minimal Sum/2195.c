int cmp(const void*a,const void*b){
    return *(int*)a-*(int*)b;
}
long long minimalKSum(int* nums, int numsSize, int k){
    qsort(nums,numsSize,sizeof(nums[0]),cmp);
    int *newnums = calloc((numsSize+1),sizeof(int));
    int nnidx = 1;
    long long ans = 0;
    long long temp = 1;
    if(k<nums[0])
        return ((long long)k*(k+1))/2;
    else{
        ans += ((long long)nums[0]*(nums[0]-1))/2;
        k-=nums[0]-1;
    }
        
    for(int i=1;i<numsSize;i++){
        if(nums[i]==nums[i-1]||nums[i]==nums[i-1]-1)
            continue;
            //nums[i-1]+1,...,nums[i]-1//
        if(k>nums[i]-nums[i-1]-1){
            ans += ((long long)(nums[i]+nums[i-1])*(nums[i]-nums[i-1]-1))/2;
            k-=nums[i]-nums[i-1]-1;
        }
        else{
            //nums[i-1]+1,...,nums[i-1]+k
            ans += ((long long)(2*nums[i-1]+1+k)*(k))/2;
            return ans;
        }
    }
    //nums[numsSize-1]+1,...,nums[numsSize-1]+k
    ans += ((long long)(2*nums[numsSize-1]+1+k)*(k))/2;
    return ans;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
int comp(const void *a, const void *b) {
  return *(int *) a - *(int *) b;
}

long long minimalKSum(int* nums, int numsSize, int k) {
 
  qsort(nums, numsSize, sizeof(*nums), comp);
  
  long long sum = 0;
  for (int i = 0; i < numsSize && nums[i] <= k; ++i) {
    if (!i || i && nums[i] != nums[i - 1]) {
      ++k;
      sum += nums[i];
    }
  }
  
  return (long long) (1 + k) * k / 2 - sum;
}
