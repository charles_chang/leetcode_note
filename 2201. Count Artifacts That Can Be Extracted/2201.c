int digArtifacts(int n, int** artifacts, int artifactsSize, int* artifactsColSize, int** dig, int digSize, int* digColSize){
    int **map = malloc(n*sizeof(int*));
    for(int i=0;i<n;i++){
        map[i] = calloc(n,sizeof(int));
    }
    for(int i=0;i<digSize;i++)
        map[dig[i][0]][dig[i][1]] = 1;
    for(int i=1;i<n;i++){
        map[i][0]+=map[i-1][0];
        map[0][i]+=map[0][i-1];
    }
    for(int i=1;i<n;i++){
        for(int j=1;j<n;j++){
            map[i][j] += map[i-1][j]+map[i][j-1]-map[i-1][j-1];
        }
    }
    // for(int i=0;i<n;i++){
    //     for(int j=0;j<n;j++){
    //         printf("%d ",map[i][j]);
    //     }
    //     printf("\n");
    // }
    // printf("\n");
    int ans = 0;
    for(int i=0;i<artifactsSize;i++){
        int target = (artifacts[i][2]-artifacts[i][0]+1)*(artifacts[i][3]-artifacts[i][1]+1);
        if(artifacts[i][0]==0 && artifacts[i][1]==0){
            if(map[artifacts[i][2]][artifacts[i][3]]==target)
                ans++;
        }
        else if(artifacts[i][0]==0){
            if(map[artifacts[i][2]][artifacts[i][3]]-map[artifacts[i][2]][artifacts[i][1]-1]==target)
                ans++;
            //printf("%d ",map[artifacts[i][2]][artifacts[i][3]]-map[artifacts[i][2]][artifacts[i][1]-1]);
        }
        else if(artifacts[i][1]==0){
            if(map[artifacts[i][2]][artifacts[i][3]]-map[artifacts[i][0]-1][artifacts[i][3]]==target)
                ans++;
            //printf("%d ",map[artifacts[i][2]][artifacts[i][3]]-map[artifacts[i][0]-1][artifacts[i][3]]);
        }
        else{
            if(map[artifacts[i][2]][artifacts[i][3]]-map[artifacts[i][0]-1][artifacts[i][3]]-map[artifacts[i][2]][artifacts[i][1]-1]+map[artifacts[i][0]-1][artifacts[i][1]-1]==target)
                ans++;
        }
        // printf("%d ",ans);
    }
    return ans;
}
