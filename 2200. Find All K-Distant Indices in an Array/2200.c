
int* findKDistantIndices(int* nums, int numsSize, int key, int k, int* returnSize){
    int *ret = malloc(numsSize*sizeof(int));
    int *check = calloc(numsSize,sizeof(int));
    int preidx = -1;
    * returnSize = 0;
    for(int i=numsSize-1;i>=0;i--){
        if(nums[i]==key){
            check[i] = 1;
            preidx = i;
        }
        else if(preidx>=0 && preidx-i<=k)
            check[i] = 1;
    }
    preidx = -1;
    for(int i=0;i<numsSize;i++){
        if(nums[i]==key){
            check[i] = 1;
            preidx = i;
        }
        else if(preidx>=0 && i-preidx<=k)
            check[i] = 1;
        if(check[i]==1)
            ret[(* returnSize)++] = i;
    }
    return ret;
}
