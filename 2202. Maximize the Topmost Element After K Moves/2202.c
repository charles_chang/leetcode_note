int maximumTop(int* nums, int numsSize, int k){
    if(numsSize==1){
        if(k%2)
            return -1;
        else
            return nums[0];
    }
    if(k==1)
        return nums[1];
    int nmax = nums[0];
    if(k>numsSize){
        for(int i=1;i<numsSize;i++)
            nmax = fmax(nmax,nums[i]);        
    }
    else{
        for(int i=0;i<k-1;i++)
            nmax = fmax(nmax,nums[i]);
        if(k<numsSize)
            nmax = fmax(nmax,nums[k]);
    }
    return nmax;
}
