char ** cellsInRange(char * s, int* returnSize){
    int col = s[3]-s[0]+1;
    int row = s[4]-s[1]+1;
    * returnSize = col*row;
    char  **ret = malloc(* returnSize*sizeof(char*));
    int dcol = 0;
    int drow = 0;
    for(int i=0;i<* returnSize;i++){
        ret[i] = calloc(3,sizeof(char));
        ret[i][0] = s[0]+dcol;
        ret[i][1] = s[1]+drow;
        if(s[1]+drow>=s[4]){
            drow = 0;
            dcol++;
        }
        else{
            drow++;
        }
        
    }
    return ret;
}
